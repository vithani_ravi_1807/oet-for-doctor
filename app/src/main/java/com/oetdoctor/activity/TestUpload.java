package com.oetdoctor.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.oetdoctor.R;

import java.util.ArrayList;

import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.VideoInfo;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;



public class TestUpload extends AppCompatActivity {
    String urlsToDownload[] = {"http://www.africau.edu/images/default/sample.pdf",
            "http://www.africau.edu/images/default/sample.pdf",
            "http://www.africau.edu/images/default/sample.pdf"};
    int counter = 0;

    private PieChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_upload);


        chart = findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        ArrayList NoOfEmp = new ArrayList();

        NoOfEmp.add(new PieEntry(21, "Critical (C)"));
        NoOfEmp.add(new PieEntry(27, "Regular (R)"));
        NoOfEmp.add(new PieEntry(30, "Advanced (A)"));
        PieDataSet dataSet = new PieDataSet(NoOfEmp, "Number Of Employees");

        ArrayList year = new ArrayList();

        year.add("2008");
        year.add("2009");
        year.add("2010");
        year.add("2011");
        year.add("2012");
        year.add("2013");
        year.add("2014");
        year.add("2015");
        year.add("2016");
        year.add("2017");
        PieData data = new PieData(dataSet);
        chart.setData(data);
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        chart.animateXY(5000, 5000);

//        videoView.setVideoPath(videoUri).getPlayer().start();


//        startService(DownloadSongService.getDownloadService(this, "http://www.africau.edu/images/default/sample.pdf", Environment.getRootDirectory()+"/"));
    }

    public void onClick(View view) {
        VideoInfo videoInfo = new VideoInfo("https://www.radiantmediaplayer.com/media/bbb-360p.mp4")
//        VideoInfo videoInfo = new VideoInfo("https://www.youtube.com/watch?v=6wNFJIbTxNk")
                .setTitle("test video")
                .setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT) //aspectRatio
                .setShowTopBar(true)
                .setPortraitWhenFullScreen(true);//portrait when full screen

        GiraffePlayer.play(TestUpload.this, videoInfo);
        /*//todo for download file using download manager
        String url = "https://doc-0g-00-docs.googleusercontent.com/docs/securesc/hs41jnnh2aee9vfih7k5me9dom2h2890/njgvg932quppvu5vis00f84fn6cu44dc/1564228800000/09767907814616499229/00853064419005842773/1NmdZuEQmxyYgptuPJtacoFC3d8Yj8bQY?e=download&nonce=5lqq0ojendbbu&user=00853064419005842773&hash=9p3kjf9qhiti1u0hlviet7mogrk88503";
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Some descrition");
        request.setTitle("Some title");
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);*/
    }


}

