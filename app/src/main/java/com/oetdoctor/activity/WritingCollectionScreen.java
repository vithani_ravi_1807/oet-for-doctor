package com.oetdoctor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.adapter.WritingAdapter;
import com.oetdoctor.model.SubWriteModel;
import com.oetdoctor.model.WritModel;
import com.oetdoctor.utils.Config;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;
import com.oetdoctor.utils.PrefUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.oetdoctor.utils.Constants.CONFIG_CLIENT_ID;
import static com.oetdoctor.utils.Constants.CONFIG_ENVIRONMENT;

public class WritingCollectionScreen extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = WritingCollectionScreen.this;
    RecyclerView rcv_writingList;
    Toolbar toolbar_top;
    List<WritModel> writModelList = new ArrayList<>();
    CircularFillableLoaders progressView;
    WritingAdapter writingAdapter;
    SubWriteModel model;
    WritModel mWritModel;
    boolean ifDataLoaded = false;



    public static final int PAYPAL_REQUEST_CODE = 123;
    // note that these credentials will differ between live & sandbox environments.

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("IELTS Helth")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    String exam;
    String caseNoteId;
    SubWriteModel selectedModel;

    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_writing_collection_screen);
        findViewById();
        toolBar();

    }

    private void findViewById() {
        rootLayout = findViewById(R.id.rootLayout);
        rcv_writingList = findViewById(R.id.rcv_writingList);
        toolbar_top = findViewById(R.id.toolbar_top);
        progressView = findViewById(R.id.progressView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv_writingList.setLayoutManager(mLayoutManager);
        rcv_writingList.setItemAnimator(new DefaultItemAnimator());
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Writing Correction");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.writing_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.writing_done:
                if (Constants.isInternetAvailable(context)) {
                    if (ifDataLoaded) {
                        ifDataLoaded=false;
                        context.startActivity(new Intent(context, SubWritingScreen.class).putExtra("exam", mWritModel.getExam()));
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void getData() {
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "getcasenote", null, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("result")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            mWritModel = new WritModel();
                            if (object.has("id")) {
                                mWritModel.setId(object.getString("id"));
                            }
                            if (object.has("main_title")) {
                                mWritModel.setMain_title(object.getString("main_title"));
                            }
                            if (object.has("exam")) {
                                mWritModel.setExam(object.getString("exam"));
                            }
                            if (object.has("preview")) {
                                mWritModel.setPreview(object.getString("preview"));
                            }
                            JSONArray jsonProductArray = object.getJSONArray("product");
                            ArrayList<SubWriteModel> subWriteModelList = new ArrayList<>();
                            for (int j = 0; j < jsonProductArray.length(); j++) {
                                model = new SubWriteModel();
                                JSONObject objectProduct = jsonProductArray.getJSONObject(j);
                                if (objectProduct.has("title")) {
                                    model.setTitle(objectProduct.getString("title"));
                                }
                                if (objectProduct.has("product_id")) {
                                    model.setProductId(objectProduct.getString("product_id"));
                                }
                                if (objectProduct.has("decription")) {
                                    model.setDescription(objectProduct.getString("decription"));
                                }
                                if (objectProduct.has("payment_type")) {
                                    model.setPaymentType(objectProduct.getString("payment_type"));
                                }
                                if (objectProduct.has("price")) {
                                    model.setPrice(objectProduct.getString("price"));
                                }
                                subWriteModelList.add(model);
                            }
                            mWritModel.setSubWriteModelList(subWriteModelList);
                            writModelList.add(mWritModel);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAdapter();
            }

            @Override
            public void onStart() {

                progressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop(){
                ifDataLoaded = true;
                progressView.setVisibility(View.GONE);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.isInternetAvailable(context)) {
            writModelList.clear();
            getData();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setAdapter() {
        writingAdapter = new WritingAdapter(writModelList, this, new OnClickProduct() {
            @Override
            public void onCLickProduct(SubWriteModel model,WritModel writModel,String id,String exam) {
                selectedModel = model;
                caseNoteId=writModel.getId();
                showTestInfoDialog( model.getTitle(),model.getPrice());
            }
        });
        rcv_writingList.setAdapter(writingAdapter);
    }

    private void showTestInfoDialog( final String title, final String price) {

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.test_info_dilog, viewGroup, false);
        Button btn_buy = dialogView.findViewById(R.id.btn_buy);
        TextView tv_dialogTitle = dialogView.findViewById(R.id.tv_dialogTitle);
        TextView tv_termAndCondition = dialogView.findViewById(R.id.tv_termAndCondition);
        final CircularFillableLoaders progressView = dialogView.findViewById(R.id.progressView);
        tv_termAndCondition.setPaintFlags(tv_termAndCondition.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        TextView tv_privacyPolicy = dialogView.findViewById(R.id.tv_privacyPolicy);
        TextView tv_price = dialogView.findViewById(R.id.tv_price);
        tv_price.setText("\u00a3" + price);
        tv_privacyPolicy.setPaintFlags(tv_privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_dialogTitle.setText(title);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    if(price == null || price.isEmpty()|| price.equals("0")){
                        postDataForPayment(caseNoteId,selectedModel.getPaymentType(),exam);
                    }else {
                        getPayment(price);
                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_termAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.termsAndConditions));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.privacyPolicy));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void getPayment(String price) {
        if(price.isEmpty()){
            price = "0";
        }
        //Getting the amount from editText
        String paymentAmount = price;

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "GBP", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        postDataForPayment(caseNoteId,selectedModel.getPaymentType(),exam);

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }


    void postDataForPayment(String id, String paymentType, final String exam) {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, context);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("case_note_id", id);
        hashMap.put("payment_type", paymentType);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "insertcasenotepayment", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    if (result != null && !result.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            if (Constants.isInternetAvailable(context)) {
                                startActivity(new Intent(context, SubWritingScreen.class).putExtra("exam", exam));
                            } else {
                                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {

                progressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop(){

                progressView.setVisibility(View.GONE);

            }
        });

    }

    public interface OnClickProduct {
        void onCLickProduct(SubWriteModel model,WritModel writModel,String id,String exam);
    }
}
