package com.oetdoctor.activity;

import android.app.Activity;
import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;
import com.oetdoctor.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.oetdoctor.utils.Config.BASE_URL;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    EditText et_currentPassword, et_newPassword, et_confirmPassword;
    TextInputLayout tl_currentPassword, tl_newPassword, tl_confirmPassword;
    Button btn_setPassword;
    String userId;

    String TAG = getClass().getSimpleName();
    Activity context = ChangePassword.this;

    CircularFillableLoaders progressView;

    LinearLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        findViewById();
    }

    private void findViewById() {
        et_currentPassword = findViewById(R.id.et_currentPassword);
        rootView = findViewById(R.id.rootView);
        progressView = findViewById(R.id.progressView);
        et_newPassword = findViewById(R.id.et_newPassword);
        et_confirmPassword = findViewById(R.id.et_confirmPassword);
        tl_currentPassword = findViewById(R.id.tl_currentPassword);
        tl_newPassword = findViewById(R.id.tl_newPassword);
        tl_confirmPassword = findViewById(R.id.tl_confirmPassword);
        btn_setPassword = findViewById(R.id.btn_setPassword);
        btn_setPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_setPassword) {
            if (Constants.isInternetAvailable(context)) {
                if (et_currentPassword.getText().length() > 0) {
                    tl_currentPassword.setErrorEnabled(false);
                    if (et_newPassword.getText().length() > 0) {
                        tl_newPassword.setErrorEnabled(false);
                        if (et_confirmPassword.getText().length() > 0) {
                            tl_confirmPassword.setErrorEnabled(false);
                            if (et_newPassword.getText().toString().equals(et_confirmPassword.getText().toString())) {
                                try {
                                    changePasswordMethod(et_currentPassword.getText().toString(), et_newPassword.getText().toString(), et_confirmPassword.getText().toString(), userId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                tl_confirmPassword.setError("Confirm password not match");
                            }
                        } else {
                            tl_confirmPassword.setError("Enter your confirm password");
                        }
                    } else {
                        tl_newPassword.setError("Enter your new password");
                    }
                } else {
                    tl_currentPassword.setError("Enter your current password");
                }
            }
        } else {
            Snackbar.make(rootView, "No Internet Connection", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        clickFalse(View.GONE, true);
    }

    private void changePasswordMethod(String currentPassword, String newPassword, String confirmPassword, String userId) throws JSONException {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        userId = PrefUtils.getStringPref(Constants.userIdKey, context);
        hashMap.put("user_id", userId);
        hashMap.put("password", newPassword);
        hashMap.put("old_password", currentPassword);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "updatepassword", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status) {
                            PrefUtils.putStringPref(Constants.passwordKey, et_newPassword.getText().toString(), context);
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                            }
                            Intent intent = new Intent(context, MainScreen.class);
                            startActivity(intent);
                        } else {

                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {

                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                clickFalse(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                clickFalse(View.GONE, true);
            }
        });


    }

    private void clickFalse(int visible, boolean b) {
        progressView.setVisibility(visible);
        btn_setPassword.setClickable(b);
        et_currentPassword.setClickable(b);
        et_newPassword.setClickable(b);
        et_confirmPassword.setClickable(b);
    }
}
