package com.oetdoctor.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.oetdoctor.R;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.PrefUtils;

public class SplashScreen extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    Activity context= SplashScreen.this;

    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final String userId = PrefUtils.getStringPref(Constants.userIdKey,context);
        String userName = PrefUtils.getStringPref(Constants.userNameKey,context);
        String password = PrefUtils.getStringPref(Constants.passwordKey,context);


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if(userId != null && !userId.isEmpty()){
                    Intent mainIntent = new Intent(context, MainScreen.class);
                    startActivity(mainIntent);
                    finish();
                }else{
                    Intent mainIntent = new Intent(context, LoginScreen.class);
                    startActivity(mainIntent);
                    finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
