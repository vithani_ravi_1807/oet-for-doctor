package com.oetdoctor.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.adapter.ExamTipsAdapter;
import com.oetdoctor.model.ExamTipsModel;
import com.oetdoctor.utils.Config;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.ItemClickListener;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;
import com.oetdoctor.utils.PrefUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.oetdoctor.utils.Constants.CONFIG_CLIENT_ID;
import static com.oetdoctor.utils.Constants.CONFIG_ENVIRONMENT;

public class GrammarBoosterMenuScreen extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = GrammarBoosterMenuScreen.this;
    Toolbar toolbar_top;
    RecyclerView rcv_testRound;
    private List<ExamTipsModel> examTipsList = new ArrayList<>();
    ExamTipsAdapter mExamTipsAdapter;

    CircularFillableLoaders mGeometricProgressView;

    public static final int PAYPAL_REQUEST_CODE = 123;
    // note that these credentials will differ between live & sandbox environments.

    String productId, title, time, audio_link;
    String quizId;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("IELTS Helth")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    private ExamTipsModel onClickStoreTipModel;

    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_exam_menu_screen);
        findViewById();
        toolBar();
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Grammar Booster");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.isInternetAvailable(context)) {
            examTipsList.clear();
            getExamTips();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void findViewById() {
        rcv_testRound = findViewById(R.id.rcv_testRound);
        rootLayout = findViewById(R.id.rootLayout);
        toolbar_top = findViewById(R.id.toolbar_top);
        mGeometricProgressView = findViewById(R.id.progressView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv_testRound.setLayoutManager(mLayoutManager);
        rcv_testRound.setItemAnimator(new DefaultItemAnimator());


    }

    void getExamTips() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("exams", "1");
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "getquiz", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (jsonObject.has("result")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ExamTipsModel mExamTipsModel = new ExamTipsModel();
                                if (object.has("id")) {
                                    mExamTipsModel.setId(object.getString("id"));
                                }
                                if (object.has("end_mock_test")) {
                                    mExamTipsModel.setEnd_mock_test(object.getBoolean("end_mock_test"));
                                }
                                if (object.has("product_id")) {
                                    mExamTipsModel.setProduct_id(object.getString("product_id"));
                                }
                                if (object.has("audio_link")) {
                                    mExamTipsModel.setAudio_link(object.getString("audio_link"));
                                }
                                if (object.has("title")) {
                                    mExamTipsModel.setTitle(object.getString("title"));
                                }
                                if (object.has("time")) {
                                    mExamTipsModel.setTime(object.getString("time"));
                                }
                                if (object.has("note")) {
                                    mExamTipsModel.setNote(object.getString("note"));
                                }
                                if (object.has("price")) {
                                    mExamTipsModel.setPrice(object.getString("price"));
                                }
                                if (object.has("payment")) {
                                    mExamTipsModel.setPayment(object.getBoolean("payment"));
                                }
                                examTipsList.add(mExamTipsModel);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAdapter();
            }

            @Override
            public void onStart() {

                mGeometricProgressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop() {

                mGeometricProgressView.setVisibility(View.GONE);
            }
        });

    }

    private void setAdapter() {
        mExamTipsAdapter = new ExamTipsAdapter(examTipsList, this, true, new ItemClickListener() {
            @Override
            public void onItemClick(View view, int position, boolean isFromGameBooster, ExamTipsModel mExamTipsModel) {

                onClickStoreTipModel = mExamTipsModel;
                productId = mExamTipsModel.getProduct_id();
                title = mExamTipsModel.getTitle();
                time = mExamTipsModel.getTime();
                audio_link = mExamTipsModel.getAudio_link();

                if (isFromGameBooster) {
                    if (mExamTipsModel.isEnd_mock_test()) {
                        startActivity(new Intent(context, ResultScreen.class).putExtra(Constants.quizId, mExamTipsModel.getId()));
                    } else {
                        quizId = mExamTipsModel.getId();
                        if (mExamTipsModel.isPayment() || !mExamTipsModel.getPrice().equals("0")) {
                            showTestInfoDialog(view, mExamTipsModel.getTitle(), mExamTipsModel.getProduct_id(), mExamTipsModel.getTime(), mExamTipsModel.getAudio_link(), mExamTipsModel.getPrice());
                        } else {
                            start(mExamTipsModel);
                        }
                    }
                }
            }
        });
        rcv_testRound.setAdapter(mExamTipsAdapter);
    }

    private void showTestInfoDialog(View view, final String title, final String productId, final String time, final String audio_link, final String price) {
        this.productId = productId;
        this.title = title;
        this.time = time;
        this.audio_link = audio_link;
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = view.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.test_info_dilog, viewGroup, false);
        Button btn_buy = dialogView.findViewById(R.id.btn_buy);
        TextView tv_dialogTitle = dialogView.findViewById(R.id.tv_dialogTitle);
        TextView tv_termAndCondition = dialogView.findViewById(R.id.tv_termAndCondition);
        final CircularFillableLoaders progressView = dialogView.findViewById(R.id.progressView);
        tv_termAndCondition.setPaintFlags(tv_termAndCondition.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        TextView tv_privacyPolicy = dialogView.findViewById(R.id.tv_privacyPolicy);
        TextView tv_price = dialogView.findViewById(R.id.tv_price);
        tv_price.setText("\u00a3" + price);
        tv_privacyPolicy.setPaintFlags(tv_privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_dialogTitle.setText(title);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    getPayment(price);

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_termAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.termsAndConditions));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.privacyPolicy));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void getPayment(String price) {
        //Getting the amount from editText
        String paymentAmount = price;

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "GBP", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        updatePaymentStatus();


                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    void updatePaymentStatus() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", userId);
        hashMap.put("quiz_id", quizId);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "quizpayment", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                JSONObject jsonObject = new JSONObject(result);
                Boolean status = jsonObject.getBoolean("status");
                if (status) {
                    start(onClickStoreTipModel);
                }
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onStop() {

            }
        });

    }

    void start(ExamTipsModel mExamTipsModel) {
        //Starting a new activity for the payment details and also putting the payment details with intent

        Intent intent = new Intent(context, ArticleScreen.class);
        intent.putExtra("tipsModel", mExamTipsModel);
        intent.putExtra("isFor", productId);
        intent.putExtra(Constants.testTitle, title);
        intent.putExtra(Constants.testTime, Integer.parseInt(time));
        intent.putExtra(Constants.audioLink, audio_link);
        context.startActivity(intent);
    }


}
