package com.oetdoctor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.oetdoctor.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import tcking.github.com.giraffeplayer2.GiraffePlayer;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class DescriptionActivity extends AppCompatActivity {

    boolean isFromVideo = false;
    String description;
    String url;
    String title;

    TextView txtDescription;
    Button btnPractice;

    Activity context = DescriptionActivity.this;

    Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        toolBar = findViewById(R.id.toolBar);
        btnPractice = findViewById(R.id.btnPractice);
        txtDescription = findViewById(R.id.txtDescription);

        getIntentData();

        toolBar.setTitle(title);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtDescription.setText(description);

        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFromVideo){
                    videoList(url);
                }else{
                    downLoadFileUsingVolley(url);
                }
            }
        });
    }

    void getIntentData(){
        description = getIntent().getStringExtra("description");
        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        isFromVideo = getIntent().getBooleanExtra("isFromVideo",false);
    }

    void downLoadFileUsingVolley(final String mUrl) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            String timestamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
            String fileName = mUrl.substring(mUrl.lastIndexOf('/') + 1, mUrl.length());
            fileName = timestamp + "_" + fileName;
            String url = mUrl;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setTitle(fileName);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }else{
            ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
        }
    }

    void videoList(String videoUrl){
        VideoInfo videoInfo = new VideoInfo(videoUrl)
                .setTitle(title)
                .setAspectRatio(VideoInfo.AR_ASPECT_FIT_PARENT) //aspectRatio
                .setShowTopBar(true)
                .setPortraitWhenFullScreen(true);//portrait when full screen

        GiraffePlayer.play(context, videoInfo);
    }

}
