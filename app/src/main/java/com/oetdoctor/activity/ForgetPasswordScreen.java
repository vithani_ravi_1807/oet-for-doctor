package com.oetdoctor.activity;

import android.app.Activity;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.BuildConfig;
import com.oetdoctor.R;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.oetdoctor.utils.Config.BASE_URL;

public class ForgetPasswordScreen extends AppCompatActivity implements View.OnClickListener {
    EditText et_userName;
    Button btn_Login;
    TextInputLayout textInputEmailAddress;

    String TAG = getClass().getSimpleName();
    Activity context = ForgetPasswordScreen.this;
    CircularFillableLoaders progressView;

    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_screen);
        findViewById();
    }

    private void findViewById() {
        rootLayout = findViewById(R.id.rootLayout);
        et_userName = findViewById(R.id.et_userName);
        btn_Login = findViewById(R.id.btn_Login);
        progressView = findViewById(R.id.progressView);
        textInputEmailAddress = findViewById(R.id.textInputEmailAddress);
        btn_Login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (Constants.isInternetAvailable(context)) {
            if (view.getId() == R.id.btn_Login) {
                if (et_userName.getText().length() > 0) {
                    textInputEmailAddress.setErrorEnabled(false);
                    forgetMethod(et_userName.getText().toString());
                } else {
                    textInputEmailAddress.setError("Enter Your Email");
                }
            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void forgetMethod(String emailName) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("email", emailName);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "login", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                if (BuildConfig.DEBUG)Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status) {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(context, "Invalid Email", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Email", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);

            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);
            }
        });

    }

    private void enableOrDisable(int visible, boolean b) {
        progressView.setVisibility(visible);
        btn_Login.setClickable(b);
        et_userName.setClickable(b);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrDisable(View.GONE, true);
    }
}
