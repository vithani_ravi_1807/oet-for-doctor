package com.oetdoctor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.adapter.BookListAdapter;
import com.oetdoctor.adapter.WritingSubAdapter;
import com.oetdoctor.model.BookListModel;
import com.oetdoctor.model.SubWriteModel;
import com.oetdoctor.model.SubWritingCorrectionModel;
import com.oetdoctor.model.WritModel;
import com.oetdoctor.utils.Config;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;
import com.oetdoctor.utils.PrefUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;

import static com.oetdoctor.utils.Constants.CONFIG_CLIENT_ID;
import static com.oetdoctor.utils.Constants.CONFIG_ENVIRONMENT;

public class BookListScreen extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = BookListScreen.this;

    LinearLayout layoutMain;
    LinearLayout rootLayout;
    LinearLayout layoutNoInternet;
    LinearLayout layoutError;
    LinearLayout emptyScreen;

    Button btnRetry;

    Toolbar toolBar;

    RecyclerView recyclerView;

    CircularFillableLoaders progressView;

    ArrayList<BookListModel> bookList = new ArrayList<>();


    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("IELTS Helth")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    BookListModel selectedModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list_screen);
        setId();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (Constants.isInternetAvailable(context)) {
            getData();
        } else {
            layoutMain.setVisibility(View.GONE);
            layoutError.setVisibility(View.GONE);
            layoutNoInternet.setVisibility(View.VISIBLE);
            btnRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recreate();
                }
            });
        }

    }

    void setId() {
        layoutMain = findViewById(R.id.layoutMain);
        rootLayout = findViewById(R.id.rootLayout);
        layoutNoInternet = findViewById(R.id.layoutNoInternet);
        layoutError = findViewById(R.id.errorLayout);
        btnRetry = findViewById(R.id.btnRetry);
        toolBar = findViewById(R.id.toolBar);
        recyclerView = findViewById(R.id.recyclerView);
        progressView = findViewById(R.id.progressView);
        emptyScreen = findViewById(R.id.emptyScreen);
    }

    void getData() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, context);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", userId);
        new JSONHelper(context, Config.BASE_URL + "getbook", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("result")) {
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            if (jsonObject.has("result")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("result");
                                bookList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    BookListModel model = new BookListModel();
                                    JSONObject object = jsonArray.optJSONObject(i);
                                    if (object.has("id")) {
                                        model.setId(object.getString("id"));
                                    }
                                    if (object.has("title")) {
                                        model.setTitle(object.getString("title"));
                                    }
                                    if (object.has("image")) {
                                        model.setImage(object.getString("image"));
                                    }
                                    if (object.has("pdf_file")) {
                                        model.setPdfFile(object.getString("pdf_file"));
                                    }
                                    if (object.has("price")) {
                                        model.setPrice(object.getString("price"));
                                    }
                                    if (object.has("description")) {
                                        model.setDescription(object.getString("description"));
                                    }
                                    if (object.has("payment")) {
                                        model.setPayment(object.getBoolean("payment"));
                                    }
                                    bookList.add(model);
                                }
                            }
                        } else {
                            layoutMain.setVisibility(View.GONE);
                            layoutError.setVisibility(View.VISIBLE);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAdapter();
            }

            @Override
            public void onStart() {
                progressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop() {
                progressView.setVisibility(View.GONE);
            }
        });

    }

    void setAdapter() {
        if (bookList.size() == 0) {
            emptyScreen.setVisibility(View.VISIBLE);
            layoutMain.setVisibility(View.GONE);
        } else {
            emptyScreen.setVisibility(View.GONE);
            layoutMain.setVisibility(View.VISIBLE);
            BookListAdapter adapter = new BookListAdapter(bookList, context, new OnClickItemListener() {
                @Override
                public void onItemClick(BookListModel model) {
                    selectedModel = model;
                    if (model.getPrice() == null || model.getPrice().isEmpty() || model.getPrice().equals("0") || model.isPayment()) {
                        setPaymentStatusDoneAndDownloadFile();
                    }else{
                        showTestInfoDialog(model.getTitle(), model.getPrice());
                    }
                }
            });
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapter);
        }
    }


    private void showTestInfoDialog(final String title, final String price) {

        ViewGroup viewGroup = findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(context).inflate(R.layout.test_info_dilog, viewGroup, false);
        Button btn_buy = dialogView.findViewById(R.id.btn_buy);
        TextView tv_dialogTitle = dialogView.findViewById(R.id.tv_dialogTitle);
        TextView tv_termAndCondition = dialogView.findViewById(R.id.tv_termAndCondition);
        TextView tv_privacyPolicy = dialogView.findViewById(R.id.tv_privacyPolicy);
        TextView tv_price = dialogView.findViewById(R.id.tv_price);


        tv_dialogTitle.setText(title);
        tv_price.setText("\u00a3" + price);

        tv_termAndCondition.setPaintFlags(tv_termAndCondition.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_privacyPolicy.setPaintFlags(tv_privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    if (price == null || price.isEmpty() || price.equals("0") || selectedModel.isPayment()) {
                        setPaymentStatusDoneAndDownloadFile();
                    } else {
                        getPayment(selectedModel.getPrice());
                    }

                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_termAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.termsAndConditions));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tv_privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.isInternetAvailable(context)) {
                    alertDialog.dismiss();
                    context.startActivity(new Intent(context, WebViewScreen.class).putExtra("isFor", Constants.privacyPolicy));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    void setPaymentStatusDoneAndDownloadFile() {

        String userId = PrefUtils.getStringPref(Constants.userIdKey, context);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("user_id", userId);
        hashMap.put("book_id", selectedModel.getId());
        new JSONHelper(context, Config.BASE_URL + "book_payment", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            if(jsonObject.has("message")) {
                                Snackbar.make(rootLayout, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show();
                            }
                            if(selectedModel.getPdfFile() != null && !selectedModel.getPdfFile().isEmpty()) {
                                downLoadFileUsingVolley(selectedModel.getPdfFile());
                            }
                        } else {
                            if(jsonObject.has("message")) {
                                Snackbar.make(rootLayout, jsonObject.getString("message"), Snackbar.LENGTH_LONG).show();
                            }
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAdapter();
            }

            @Override
            public void onStart() {
                progressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop() {
                progressView.setVisibility(View.GONE);
            }
        });
    }

    void downLoadFileUsingVolley(final String mUrl) {
        Intent intent = new Intent(context,DescriptionActivity.class);
        intent.putExtra("title",selectedModel.getTitle());
        intent.putExtra("url",mUrl);
        intent.putExtra("description",selectedModel.getDescription());
        intent.putExtra("isFromVideo",false);
        startActivity(intent);
       /* if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            String timestamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
            String fileName = mUrl.substring(mUrl.lastIndexOf('/') + 1, mUrl.length());
            fileName = timestamp + "_" + fileName;
            String url = mUrl;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setTitle(fileName);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }else{
            ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
        }*/
    }

    private void getPayment(String price) {
        if (price.isEmpty()) {
            price = "0";
        }
        //Getting the amount from editText
        String paymentAmount = price;

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "GBP", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, 1111);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == 1111) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        setPaymentStatusDoneAndDownloadFile();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    public interface OnClickItemListener {
        void onItemClick(BookListModel model);
    }
}
