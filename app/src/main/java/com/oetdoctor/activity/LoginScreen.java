package com.oetdoctor.activity;

import android.app.Activity;
import android.content.Intent;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.BuildConfig;
import com.oetdoctor.R;
import com.oetdoctor.model.UserModel;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;
import com.oetdoctor.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.oetdoctor.utils.Config.BASE_URL;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {

    String TAG = getClass().getSimpleName();
    Activity context = LoginScreen.this;

    EditText editUserName, editPassword;
    TextView txtForgotPassword, txtSignUp;
    TextInputLayout textInputUserName, textInputPassword;
    Button btnLogin;
    Button btnRetry;

    CircularFillableLoaders geometricProgressView;

    LinearLayout layoutNoInternet;
    LinearLayout layoutError;
    FrameLayout layoutMain;
    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        findViewById();
        onClickEvent();
        if (!Constants.isInternetAvailable(context)) {
            layoutNoInternet.setVisibility(View.VISIBLE);
            layoutMain.setVisibility(View.GONE);
        }
    }

    private void findViewById() {
        rootLayout = findViewById(R.id.rootLayout);
        editUserName = findViewById(R.id.et_userName);
        editPassword = findViewById(R.id.et_passWord);
        txtForgotPassword = findViewById(R.id.tv_forgotPassword);
        txtSignUp = findViewById(R.id.tv_btnSigup);
        btnLogin = findViewById(R.id.btn_Login);
        textInputUserName = findViewById(R.id.tl_username);
        textInputPassword = findViewById(R.id.tl_password);
        geometricProgressView = findViewById(R.id.progressView);
        btnRetry = findViewById(R.id.btnRetry);
        layoutNoInternet = findViewById(R.id.layoutNoInternet);
        layoutError = findViewById(R.id.layoutError);
        layoutMain = findViewById(R.id.layoutMain);
    }

    private void onClickEvent() {
        btnLogin.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);
        btnRetry.setOnClickListener(this);
    }

    void callLoginApi(String username, String password) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("email", username);
        hashMap.put("password", password);
        new JSONHelper(context, BASE_URL + "login", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                if (BuildConfig.DEBUG) {

                    if (BuildConfig.DEBUG) Log.i(TAG, "onResult: " + result);
                }
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status && object.has("result")) {
                            JSONObject jsonObject = object.getJSONObject("result");
                            UserModel model = new UserModel();
                            if (jsonObject.has("id")) {
                                model.setId(jsonObject.getString("id"));
                            }
                            if (jsonObject.has("email")) {
                                model.setEmail(jsonObject.getString("email"));
                            }
                            if (jsonObject.has("account_name")) {
                                model.setAccount_name(jsonObject.getString("account_name"));
                            }
                            if (jsonObject.has("registered")) {
                                model.setRegistered(jsonObject.getString("registered"));
                            }
                            if (jsonObject.has("user_status")) {
                                model.setUserStatus(jsonObject.getString("user_status"));
                            }
                            if (jsonObject.has("phone")) {
                                model.setPhone(jsonObject.getString("phone"));
                            }
                            if (jsonObject.has("country_name")) {
                                model.setCountryName(jsonObject.getString("country_name"));
                            }
                            if (jsonObject.has("qualification")) {
                                model.setQualification(jsonObject.getString("qualification"));
                            }
                            if (jsonObject.has("profile")) {
                                model.setProfile(jsonObject.getString("profile"));
                            }

                            PrefUtils.putStringPref(Constants.userIdKey, model.getId(), context);
                            PrefUtils.putStringPref(Constants.userNameKey, editUserName.getText().toString(), context);
                            PrefUtils.putStringPref(Constants.userImage, model.getProfile().toString(), context);
                            PrefUtils.putStringPref(Constants.passwordKey, editPassword.getText().toString(), context);

                            Intent intent = new Intent(context, MainScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            if (object.has("message") && object.getString("message") != null) {
//                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                Snackbar.make(rootLayout, object.getString("message"), Snackbar.LENGTH_LONG).show();
                            } else {
//                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                                Snackbar.make(rootLayout, object.getString("Invalid Username or Password"), Snackbar.LENGTH_LONG).show();
                            }
                        }
                    } else {
//                        Toast.makeText(context, getResources().getString(R.string.errorToast), Toast.LENGTH_SHORT).show();
                        Snackbar.make(rootLayout, object.getString(getResources().getString(R.string.errorToast)), Snackbar.LENGTH_LONG).show();
                    }
                } else {
//                    Toast.makeText(context, getResources().getString(R.string.errorToast), Toast.LENGTH_SHORT).show();
                    Snackbar.make(rootLayout, getResources().getString(R.string.errorToast), Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);

            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, true);

            }
        });

    }

    private void enableOrDisable(int visible, boolean b) {
        geometricProgressView.setVisibility(visible);
        editUserName.setClickable(b);
        editPassword.setClickable(b);
        txtForgotPassword.setClickable(b);
        txtSignUp.setClickable(b);
        btnLogin.setClickable(b);
    }

    private void loginMethod() {
        textInputUserName.setErrorEnabled(false);
        textInputPassword.setErrorEnabled(false);

        if (!editUserName.getText().toString().isEmpty()) {
            if (!editPassword.getText().toString().isEmpty()) {
                callLoginApi(editUserName.getText().toString(), editPassword.getText().toString());
            } else {
                textInputPassword.setErrorEnabled(true);
                textInputPassword.setError("Enter Password");
            }
        } else {
            textInputUserName.setErrorEnabled(true);
            textInputUserName.setError("Enter Username");
        }
    }

    private void forgotPasswordMethod() {

        startActivity(new Intent(this, ForgetPasswordScreen.class));
    }

    private void sigUpMethod() {
        startActivity(new Intent(this, SignUpScreen.class));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_Login:
                if (Constants.isInternetAvailable(context)) {
                    loginMethod();
                } else {

                    Snackbar.make(rootLayout, "No Internet Connection", Snackbar.LENGTH_LONG).show();

//                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_forgotPassword:
                if (Constants.isInternetAvailable(context)) {
                    forgotPasswordMethod();
                } else {
                    Snackbar.make(rootLayout, "No Internet Connection", Snackbar.LENGTH_LONG).show();
//                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_btnSigup:
                if (Constants.isInternetAvailable(context)) {
                    sigUpMethod();
                } else {
                    Snackbar.make(rootLayout, "No Internet Connection", Snackbar.LENGTH_LONG).show();
//                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnRetry:
                recreate();
                break;

            default:
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrDisable(View.GONE, true);

    }

}
