package com.oetdoctor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.oetdoctor.R;
import com.oetdoctor.adapter.ChatAdapter;
import com.oetdoctor.model.ChatModel;
import com.oetdoctor.utils.Config;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.JSONHelper;
import com.oetdoctor.utils.OnAsyncLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChatLayoutScreen extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    Activity context = ChatLayoutScreen.this;

    String supportId;
    Toolbar toolbar_top;
    List<ChatModel> chatModelList = new ArrayList<>();
    ChatAdapter chatAdapter;
    RecyclerView rcv_chat;
    ImageView iv_send;
    EditText et_message;

    LinearLayout rootLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_layout_screen);
        findViewById();
        toolBar();

        if (Constants.isInternetAvailable(context)) {
            getData();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


        iv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    String msgContent = et_message.getText().toString();
                    if (!TextUtils.isEmpty(msgContent)) {
                        try {
                            postReplay(msgContent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        et_message.setText("");
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void findViewById() {
        rootLayout = findViewById(R.id.rootLayout);
        toolbar_top = findViewById(R.id.toolbar_top);
        rcv_chat = findViewById(R.id.rcv_chat);
        iv_send = findViewById(R.id.iv_send);
        et_message = findViewById(R.id.et_message);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv_chat.setLayoutManager(mLayoutManager);
        rcv_chat.setItemAnimator(new DefaultItemAnimator());
        supportId = getIntent().getStringExtra(Constants.supportId);
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Support Message");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    void getData() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
//        hashMap.put("support_id", "2");
        hashMap.put("support_id", supportId);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "getsupportreply", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (jsonObject.has("result")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ChatModel mExamTipsModel = new ChatModel();
                                if (object.has("id")) {
                                    mExamTipsModel.setId(object.getString("id"));
                                }
                                if (object.has("status")) {
                                    mExamTipsModel.setStatus(object.getString("status"));
                                }
                                if (object.has("message")) {
                                    mExamTipsModel.setMessage(object.getString("message"));
                                }
                                if (object.has("created_date")) {
                                    mExamTipsModel.setCreated_date(object.getString("created_date"));
                                }
                                chatModelList.add(mExamTipsModel);
                            }
                        }
                    }
                    setAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {


            }

            @Override
            public void onStop() {

            }
        });
//
    }


    private void postReplay(String msgContent) throws JSONException {
        HashMap<String, String> hashMap = new HashMap<String, String>();
//        hashMap.put("support_id", "2");
        hashMap.put("support_id", supportId);
        hashMap.put("message", msgContent);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "submitreply", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (jsonObject.has("result")) {
                            chatModelList = new ArrayList<>();
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ChatModel mExamTipsModel = new ChatModel();
                                if (object.has("id")) {
                                    mExamTipsModel.setId(object.getString("id"));
                                }
                                if (object.has("status")) {
                                    mExamTipsModel.setStatus(object.getString("status"));
                                }
                                if (object.has("message")) {
                                    mExamTipsModel.setMessage(object.getString("message"));
                                }
                                if (object.has("created_date")) {
                                    mExamTipsModel.setCreated_date(object.getString("created_date"));
                                }
                                chatModelList.add(mExamTipsModel);

                            }

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAdapter();
            }

            @Override
            public void onStart() {


            }

            @Override
            public void onStop()  {

            }
        });

    }


    private void setAdapter() {
//        if (chatAdapter != null) {
//            chatAdapter.notifyDataSetChanged();
//        } else {
        chatAdapter = new ChatAdapter(chatModelList, this);
        rcv_chat.setAdapter(chatAdapter);
//        }
    }


}
