package com.oetdoctor.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oetdoctor.R;
import com.oetdoctor.model.ExamTipsModel;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.BuildConfig;

public class ArticleScreen extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtDescription;
    Button btnPractice;
    Activity context = ArticleScreen.this;

    String productId, title, audio_link;
    int time;

    LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artical_screen);

        toolbar = findViewById(R.id.toolBar);
        txtDescription = findViewById(R.id.txtDescription);
        btnPractice = findViewById(R.id.btnPractice);
        rootLayout = findViewById(R.id.rootLayout);

        productId = getIntent().getStringExtra("isFor");
        title = getIntent().getStringExtra(Constants.testTitle);
        time = getIntent().getIntExtra(Constants.testTime, 0);
        audio_link = getIntent().getStringExtra(Constants.audioLink);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final ExamTipsModel model = (ExamTipsModel) getIntent().getSerializableExtra("tipsModel");
        txtDescription.setText(model.getNote());
        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.isEnd_mock_test()) {
                    startActivity(new Intent(ArticleScreen.this, ResultScreen.class).putExtra(Constants.quizId, model.getId()));
                } else {
                    Intent intent = new Intent(context, ExamScreen.class);
                    intent.putExtra("isFor", productId);
                    intent.putExtra(Constants.testTitle, title);
                    intent.putExtra(Constants.testTime, time);
                    intent.putExtra(Constants.audioLink, audio_link);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }


}
