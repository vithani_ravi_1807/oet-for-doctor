package com.oetdoctor.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.oetdoctor.R;
import com.oetdoctor.activity.BookListScreen;
import com.oetdoctor.activity.GrammarBoosterMenuScreen;
import com.oetdoctor.activity.IELTS_Expert;
import com.oetdoctor.activity.MockExamMenuScreen;
import com.oetdoctor.activity.PlacementTestExamMenuScreen;
import com.oetdoctor.activity.TipsActivity;
import com.oetdoctor.activity.VideoListActivity;
import com.oetdoctor.activity.WritingCollectionScreen;
import com.oetdoctor.utils.Constants;

import java.net.URISyntaxException;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    CardView ll_cbtTips, ll_mockExam, ll_grammar_booster,ll_expert,ll_writing,layoutPlacementExamCard,cardPlacementTest,cardVideoList,cardBookList;
    Button btn_retry_internet;
    LinearLayout rl_noInternet;
    LinearLayout rootLayout;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        findViewById(view);
        if (Constants.isInternetAvailable(getActivity())) {
            rl_noInternet.setVisibility(View.GONE);
        } else {
            rl_noInternet.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void findViewById(View view) {
        rootLayout = view.findViewById(R.id.rootLayout);
        ll_cbtTips = view.findViewById(R.id.ll_cbtTips);
        ll_mockExam = view.findViewById(R.id.ll_mockExam);
        ll_grammar_booster = view.findViewById(R.id.ll_grammar_booster);
        cardPlacementTest = view.findViewById(R.id.cardPlacementTest);
        ll_expert = view.findViewById(R.id.ll_expert);
        ll_writing = view.findViewById(R.id.ll_writing);
        layoutPlacementExamCard = view.findViewById(R.id.layoutPlacementExamCard);
        cardVideoList = view.findViewById(R.id.cardVideoList);
        rl_noInternet = view.findViewById(R.id.layoutNoInternet);
        cardBookList = view.findViewById(R.id.cardBookList);
        btn_retry_internet = view.findViewById(R.id.btnRetry);
        btn_retry_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().recreate();
            }
        });
        ll_writing.setOnClickListener(this);
        ll_cbtTips.setOnClickListener(this);
        ll_mockExam.setOnClickListener(this);
        ll_grammar_booster.setOnClickListener(this);
        ll_expert.setOnClickListener(this);
        cardPlacementTest.setOnClickListener(this);
        layoutPlacementExamCard.setOnClickListener(this);
        cardVideoList.setOnClickListener(this);
        cardBookList.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (Constants.isInternetAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.ll_cbtTips:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intent = new Intent(getActivity(), TipsActivity.class);
                        getActivity().startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_mockExam:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), MockExamMenuScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                 case R.id.layoutPlacementExamCard:
                     try {
                         Intent myIntent = new Intent().parseUri("https://live.vcita.com/site/ieltsmedical/online-scheduling?category=6oeazs85vwpnlbsh", Intent.URI_INTENT_SCHEME);
                         startActivity(myIntent);
                         WebView mWebView = new WebView(getActivity());
                     } catch (URISyntaxException e) {
                         e.printStackTrace();
                     }
                     break;
                case R.id.ll_grammar_booster:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), GrammarBoosterMenuScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_expert:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), IELTS_Expert.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_writing:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), WritingCollectionScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.cardPlacementTest:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), PlacementTestExamMenuScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                 case R.id.cardVideoList:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), VideoListActivity.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                  case R.id.cardBookList:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), BookListScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }
}
