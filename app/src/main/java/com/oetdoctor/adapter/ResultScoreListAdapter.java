package com.oetdoctor.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.oetdoctor.R;
import com.oetdoctor.activity.BookListScreen;
import com.oetdoctor.model.BookListModel;
import com.oetdoctor.model.ResultScoreModel;
import com.oetdoctor.utils.TLSSocketFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

public class ResultScoreListAdapter extends RecyclerView.Adapter<ResultScoreListAdapter.MyViewHolder> {

    private List<ResultScoreModel> bookList;
    Context context;


    public ResultScoreListAdapter(List<ResultScoreModel> bookList, Context context) {
        this.bookList = bookList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.resultscorelayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ResultScoreModel model = bookList.get(position);
        if(model != null){

            if(model.getTitle() != null && !model.getTitle().isEmpty() && model.getPercentage() != null && !model.getPercentage().isEmpty()){
                holder.title.setText(model.getTitle()+" "+model.getPercentage());
            }else if(model.getTitle() != null && !model.getTitle().isEmpty()){
                holder.title.setText(model.getTitle());
            }else{
                holder.title.setVisibility(View.GONE);
            }


            if(model.getTotalQuestion() != null && !model.getTotalQuestion().isEmpty()){
                holder.from.setText(model.getTotalQuestion());
            }else{
                holder.from.setText("-");
            }
            if(model.getCorrectAns() != null && !model.getCorrectAns().isEmpty()){
                holder.score.setText(model.getCorrectAns());
            }else{
                holder.score.setText("-");
            }

        }
    }


    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView score;
        TextView from;

        LinearLayout priceLayout;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);
            title = view.findViewById(R.id.txtScoreTitle);
            score = view.findViewById(R.id.txtScore);
            from = view.findViewById(R.id.txtFrom);
        }
    }

}


