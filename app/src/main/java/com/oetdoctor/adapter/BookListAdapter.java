package com.oetdoctor.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.oetdoctor.R;
import com.oetdoctor.activity.BookListScreen;
import com.oetdoctor.model.BookListModel;
import com.oetdoctor.model.ResultModel;
import com.oetdoctor.utils.TLSSocketFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.MyViewHolder> {

    private List<BookListModel> bookList;
    Context context;
    BookListScreen.OnClickItemListener listener;


    public BookListAdapter(List<BookListModel> bookList, Context context,BookListScreen.OnClickItemListener listener) {
        this.bookList = bookList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booklistitem, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final BookListModel model = bookList.get(position);
        if(model != null){
            if(model.getImage() != null && !model.getImage().isEmpty()){
                new MyAsync(model.getImage(),holder.bookImage).execute();
            }else{
                holder.bookImage.setVisibility(View.GONE);
            }
            if(model.getTitle() != null && !model.getTitle().isEmpty()){
                holder.title.setText(model.getTitle());
            }else{
                holder.title.setVisibility(View.GONE);
            }
            if(model.getDescription() != null && !model.getDescription().isEmpty()){
//                holder.description.setText(model.getDescription());
            }else{
                holder.description.setVisibility(View.GONE);
            }
            if(model.getPrice() != null && !model.getPrice().isEmpty()){
                holder.price.setText("\u00a3" + model.getPrice());
            }else{
                holder.price.setVisibility(View.GONE);
            }
            if(model.isPayment()){
               holder.priceLayout.setVisibility(View.GONE);
            }

            holder.mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(model);
                }
            });

            Random rnd = new Random();
            int currentColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            holder.coloredCard.setCardBackgroundColor(currentColor);
        }else{
            holder.mainCard.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView mainCard;
        CardView coloredCard;
        ImageView bookImage;
        TextView title;
        TextView description;
        TextView price;

        LinearLayout priceLayout;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);
            mainCard = view.findViewById(R.id.cardView);
            coloredCard = view.findViewById(R.id.coloredCardView);
            bookImage = view.findViewById(R.id.bookImage);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            price = view.findViewById(R.id.price);
            priceLayout = view.findViewById(R.id.priceLayout);
        }
    }

    public class MyAsync extends AsyncTask<Void, Void, Void> {

        String url;
        ImageView imageView;

        public MyAsync(String url,ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            ImageRequest imageRequest = new ImageRequest(
                    url, // Image URL
                    new Response.Listener<Bitmap>() { // Bitmap listener
                        @Override
                        public void onResponse(final Bitmap response) {
                            try {
                                imageView.setImageBitmap(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    0, // Image width
                    0, // Image height
                    Bitmap.Config.RGB_565, //Image decode configuration
                    new Response.ErrorListener() { // Error listener
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }
            );

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                HttpStack stack = null;
                try {
                    stack = new HurlStack(null, new TLSSocketFactory());
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                    Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                    stack = new HurlStack();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                    stack = new HurlStack();
                }
                requestQueue = Volley.newRequestQueue(context, stack);
            } else {
                requestQueue = Volley.newRequestQueue(context);
            }

            requestQueue.add(imageRequest);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}


