package com.oetdoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.activity.WritingCollectionScreen;
import com.oetdoctor.model.WritModel;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.List;

public class WritingAdapter extends RecyclerView.Adapter<WritingAdapter.MyViewHolder> {

    private List<WritModel> moviesList;
    Context context;
    boolean isFromGameBooster;
    WritingCollectionScreen.OnClickProduct OnClickProduct;

    public WritingAdapter(List<WritModel> moviesList, Context context,WritingCollectionScreen.OnClickProduct OnClickProduct) {
        this.moviesList = moviesList;
        this.context = context;
        this.isFromGameBooster = isFromGameBooster;
        this.OnClickProduct = OnClickProduct;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.writing_correction, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final WritModel mWritModel = moviesList.get(position);
        holder.tv_main_title.setText(mWritModel.getMain_title());
        holder.tv_preview.setText(mWritModel.getPreview());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        ProductListForWritingAdapter adapter = new ProductListForWritingAdapter(mWritModel.getSubWriteModelList(), context,mWritModel.getId(),holder.progressView,mWritModel.getExam(),OnClickProduct,mWritModel);
        holder.recyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_main_title, tv_preview;
        CardView cv_test_click;
        RecyclerView recyclerView;
        CircularFillableLoaders progressView;

        public MyViewHolder(View view) {
            super(view);
            progressView = (CircularFillableLoaders) view.findViewById(R.id.progressView);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_preview = (TextView) view.findViewById(R.id.tv_preview);
            tv_main_title = (TextView) view.findViewById(R.id.tv_main_title);
            recyclerView =  view.findViewById(R.id.recyclerView);
        }
    }


}