package com.oetdoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.oetdoctor.R;
import com.oetdoctor.model.TipsModel;

import java.util.ArrayList;

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;

    private ArrayList<TipsModel> list;

    public CustomPagerAdapter(Context mContext, ArrayList<TipsModel> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
//        ModelObject modelObject = ModelObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.tips_card, collection, false);
        TextView txtMessage = layout.findViewById(R.id.txtMessage);
        txtMessage.setText(list.get(position).getMessage());
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position).getMessage();
    }

}