package com.oetdoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oetdoctor.R;
import com.oetdoctor.model.ChatModel;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<ChatModel> moviesList;
    Context context;


    public ChatAdapter(List<ChatModel> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ChatModel mChatModel = moviesList.get(position);
        if (mChatModel.getStatus().equals("1")) {
            holder.cv_right.setVisibility(View.GONE);
            holder.tv_left.setText(mChatModel.getMessage());
            holder.tv_date_left.setText(mChatModel.getCreated_date());
        } else {
            holder.cv_left.setVisibility(View.GONE);
            holder.tv_right.setText(mChatModel.getMessage());
            holder.tv_date_right.setText(mChatModel.getCreated_date());
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_left, tv_right, tv_date_right, tv_date_left;
        CardView cv_right, cv_left;

        public MyViewHolder(View view) {
            super(view);
            tv_left = (TextView) view.findViewById(R.id.tv_left);
            tv_right = (TextView) view.findViewById(R.id.tv_right);
            cv_right = (CardView) view.findViewById(R.id.cv_right);
            cv_left = (CardView) view.findViewById(R.id.cv_left);
            tv_date_right = (TextView) view.findViewById(R.id.tv_date_right);
            tv_date_left = (TextView) view.findViewById(R.id.tv_date_left);
        }
    }
}