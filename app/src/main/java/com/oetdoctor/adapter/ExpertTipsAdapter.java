package com.oetdoctor.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oetdoctor.R;
import com.oetdoctor.activity.ChatLayoutScreen;
import com.oetdoctor.model.ExpertTipsModel;
import com.oetdoctor.utils.Constants;

import java.util.List;

public class ExpertTipsAdapter extends RecyclerView.Adapter<ExpertTipsAdapter.MyViewHolder> {

    private List<ExpertTipsModel> tipsList;
    Context context;


    public ExpertTipsAdapter(List<ExpertTipsModel> moviesList, Context context) {
        this.tipsList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mock_test_expert_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ExpertTipsModel mExpertTipsModel = tipsList.get(position);
        holder.tv_title.setText(mExpertTipsModel.getSubject());
        holder.tv_message.setText(mExpertTipsModel.getMessage());
        holder.tv_date.setText(mExpertTipsModel.getCreated_date());
        holder.cv_test_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    context.startActivity(new Intent(context, ChatLayoutScreen.class).putExtra(Constants.supportId, mExpertTipsModel.getId()));
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return tipsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_message, tv_date;
        CardView cv_test_click;

        public MyViewHolder(View view) {
            super(view);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_message = (TextView) view.findViewById(R.id.tv_message);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
        }
    }

}