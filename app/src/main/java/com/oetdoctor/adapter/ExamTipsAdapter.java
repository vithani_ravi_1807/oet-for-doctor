package com.oetdoctor.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oetdoctor.R;
import com.oetdoctor.model.ExamTipsModel;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.ItemClickListener;

import java.util.List;
import java.util.Random;

public class ExamTipsAdapter extends RecyclerView.Adapter<ExamTipsAdapter.MyViewHolder> {

    private List<ExamTipsModel> moviesList;
    Context context;
    boolean isFromGameBooster;
    ItemClickListener clickListener;

    public ExamTipsAdapter(List<ExamTipsModel> moviesList, Context context, boolean isFromGameBooster, ItemClickListener clickListener) {
        this.moviesList = moviesList;
        this.context = context;
        this.isFromGameBooster = isFromGameBooster;
        this.clickListener = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mock_test_exams_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ExamTipsModel mExamTipsModel = moviesList.get(position);
        holder.tv_title.setText(mExamTipsModel.getTitle());
        if (!isFromGameBooster) {
            holder.tv_note.setText(mExamTipsModel.getNote());
        }
        Random rnd = new Random();
        int currentColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.cv_color.setCardBackgroundColor(currentColor);
        if (mExamTipsModel.getNote() != null && mExamTipsModel.getNote().toString() != "") {
            holder.tv_note.setVisibility(View.VISIBLE);
        } else {
            holder.tv_note.setVisibility(View.GONE);
        }
        if (mExamTipsModel.isPayment() || !mExamTipsModel.getPrice().equals("0")) {
            holder.tv_price.setText("\u00a3" + mExamTipsModel.getPrice());
        } else {
            holder.priceLayout.setVisibility(View.GONE);
        }
        holder.cv_test_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    clickListener.onItemClick(view, position, isFromGameBooster, mExamTipsModel);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_note, tv_price;
        CardView cv_color, cv_test_click;
        LinearLayout priceLayout;

        public MyViewHolder(View view) {
            super(view);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            cv_color = (CardView) view.findViewById(R.id.cv_color);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_note = (TextView) view.findViewById(R.id.tv_note);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            priceLayout = view.findViewById(R.id.priceLayout);
        }
    }


}