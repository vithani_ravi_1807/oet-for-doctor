package com.oetdoctor.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;
import com.oetdoctor.R;
import com.oetdoctor.activity.SubWritingScreen;
import com.oetdoctor.model.SubWritingCorrectionModel;
import com.oetdoctor.utils.Constants;
import com.oetdoctor.utils.InputStreamVolleyRequest;
import com.oetdoctor.utils.TLSSocketFactory;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class WritingSubAdapter extends RecyclerView.Adapter<WritingSubAdapter.MyViewHolder> {

    private List<SubWritingCorrectionModel> moviesList;
    Activity context;
    boolean isFromGameBooster;
    MyViewHolder holder;
    String fileName;
    String exam;
    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = "";
    private String url;
    ProgressDialog progress;
    SubWritingScreen.UploadImage uploadImageListener;


    public WritingSubAdapter(List<SubWritingCorrectionModel> moviesList, Activity context, String exam, SubWritingScreen.UploadImage uploadImageListener) {
        this.moviesList = moviesList;
        this.context = context;
        this.isFromGameBooster = isFromGameBooster;
        this.exam = exam;
        this.uploadImageListener = uploadImageListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.writing_sub_correction, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        this.holder = holder;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final SubWritingCorrectionModel mSubWritingCorrectionModel = moviesList.get(position);
        holder.tv_title.setText(mSubWritingCorrectionModel.getMain_title());
        holder.tv_preview.setText(mSubWritingCorrectionModel.getPreview());
        holder.commentText.setText(mSubWritingCorrectionModel.getComment());
        holder.tv_date.setText(mSubWritingCorrectionModel.getCreated_date());
        holder.gradText.setText(mSubWritingCorrectionModel.getGrade());
        holder.tv_description.setText(mSubWritingCorrectionModel.getDecription());
//        if(mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null") {


        if (mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null" && !mSubWritingCorrectionModel.getFinal_time().isEmpty()) {
            long futureTimestamp = Long.parseLong(mSubWritingCorrectionModel.getFinal_time());
            long timep = getDate(futureTimestamp);
            CountDownTimer cdt = new CountDownTimer(timep, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    /*long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.DAYS.toMillis(days);*/

                    long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                    long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                    holder.timerText.setText(hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
                }

                @Override
                public void onFinish() {
                    holder.timerText.setText("Finish!");
                }
            };
            cdt.start();
        }

        holder.btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFile_name());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.btn_feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFeedback_file());
//                    downLoadPdf("https://www.ieltsmedicalapp.com/android/img/file/190708031128.jpg");
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (mSubWritingCorrectionModel.getStatus().equals("0")) {
            holder.btn_upload.setVisibility(View.GONE);
        } else if (mSubWritingCorrectionModel.getStatus().equals("1")) {
            holder.timerText.setVisibility(View.GONE);
            holder.gradText.setVisibility(View.VISIBLE);
            holder.commentText.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.VISIBLE);
            holder.btn_upload.setVisibility(View.GONE);
        } else {
            holder.timerText.setVisibility(View.VISIBLE);
            holder.gradText.setVisibility(View.GONE);
            holder.commentText.setVisibility(View.GONE);
            holder.btn_upload.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.GONE);
        }
        holder.btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    uploadImageListener.onCLickUpload(mSubWritingCorrectionModel);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private long getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        Date currentTime1 = cal.getTime();
        Date currentTime = Calendar.getInstance().getTime();

        long diff = currentTime1.getTime() - currentTime.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        return TimeUnit.SECONDS.toMillis(seconds);
    }


    private void downLoadPdf(String file_name) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        Date now = new Date();
        fileName = formatter.format(now);
        url = file_name;
        downLoadFileUsingVolley(url);
    }




    void downLoadFileUsingVolley(final String mUrl) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            String timestamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
            fileName = mUrl.substring(mUrl.lastIndexOf('/') + 1, mUrl.length());
            fileName = timestamp + "_" + fileName;
            String url = mUrl;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setTitle(fileName);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }else{
            ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
        }
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title, tv_description, tv_date, tv_preview, gradText, commentText;
        CardView cv_test_click;
        Button btn_pdf, btn_upload, btn_feedBack;
        CircularFillableLoaders progressView;
        TextView timerText;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);

            progressView = view.findViewById(R.id.progressView);
            btn_pdf = view.findViewById(R.id.btn_pdf);
            btn_feedBack = view.findViewById(R.id.btn_feedBack);
            btn_upload = view.findViewById(R.id.btn_upload);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_preview = (TextView) view.findViewById(R.id.tv_preview);
            commentText = (TextView) view.findViewById(R.id.commentText);
            gradText = (TextView) view.findViewById(R.id.gradText);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            timerText = (TextView) view.findViewById(R.id.timerText);
        }
    }


}