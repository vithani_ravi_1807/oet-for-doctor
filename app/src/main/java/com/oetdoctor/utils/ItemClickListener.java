package com.oetdoctor.utils;

import android.view.View;

import com.oetdoctor.model.ExamTipsModel;

public interface ItemClickListener {
    void onItemClick(View view, int position, boolean isFromGameBooster, ExamTipsModel mExamTipsModel);
}