package com.oetdoctor.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import com.paypal.android.sdk.payments.PayPalConfiguration;

public class Constants {
    public static String userNameKey = "user_name";
    public static String userImage = "user_image";
    public static String passwordKey = "password";
    public static String userIdKey = "userId";
    public static String testTitle = "testTitle";
    public static String testTime = "testTime";
    public static String audioLink = "audioLink";
    public static String quizId = "quizId";
    public static String supportId = "supportId";
    public static   String CONFIG_CLIENT_ID = "AeJNACBt2oYA1Uy0rJCmD67ht3luuWqpBICFwLWVQnUaPlxZWOxzJ16lQTIJvYZY4qC0HrYVNnq8oWua";
//    public static   String CONFIG_CLIENT_ID = "AZdYBsbOE7IkcyQJHF-O3DljEhSTd-a9mH0vsz6EDpBMLy7MALsGU6rPWRFXUGsQ-TskdfB9tV4wnDD6";
    public static   String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
//    public static   String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    public static String cbtTips = "cbtTips";
    public static String termsAndConditions = "termsAndConditions";
    public static String privacyPolicy= "privacyPolicy:";

    public static boolean isInternetAvailable(Context context) {
        try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                return cm.getActiveNetworkInfo() != null;

        } catch (Exception e) {
            return false;
        }
    }
}
