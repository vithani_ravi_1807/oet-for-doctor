package com.oetdoctor.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JSONHelper {


    Context context;
    String myUrl;
    ProgressDialog progressDialog;
    OnAsyncLoader onAsyncLoader;
    HashMap<String, String> hashMap;

    public JSONHelper(Context context, String url, final HashMap<String, String> hashMap, final OnAsyncLoader onAsyncLoader){
        this.context = context;
        myUrl = url;
        this.onAsyncLoader = onAsyncLoader;
        this.hashMap = hashMap;


        onAsyncLoader.onStart();

        final ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        if (hashMap != null) {
            for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        RequestQueue requestQueue;

        int a = Request.Method.POST;
        if(url.contains("player.vimeo.com")){
            a = Request.Method.GET;
        }
        StringRequest stringRequest = new StringRequest(a, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    onAsyncLoader.onResult(response);
                    onAsyncLoader.onStop();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            onAsyncLoader.onResult(error.getMessage());
                            onAsyncLoader.onStop();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return hashMap;
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            HttpStack stack = null;
            try {
                stack = new HurlStack(null, new TLSSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            }
            requestQueue = Volley.newRequestQueue(context, stack);
        } else {
            requestQueue = Volley.newRequestQueue(context);
        }

        requestQueue.add(stringRequest);
    }
}