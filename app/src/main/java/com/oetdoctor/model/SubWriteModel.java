package com.oetdoctor.model;

import java.io.Serializable;

public class SubWriteModel implements Serializable {
    String title;
    String productId;
    String description;
    String paymentType;
    String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        return "SubWriteModel{" +
                "title='" + title + '\'' +
                ", productId='" + productId + '\'' +
                ", description='" + description + '\'' +
                ", paymentType='" + paymentType + '\'' +
                '}';
    }
}
