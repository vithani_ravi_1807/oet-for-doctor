package com.oetdoctor.model;

public class ResultScoreModel {
    String title;
    String totalQuestion;
    String correctAns;
    String percentage;


    public ResultScoreModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public String getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(String correctAns) {
        this.correctAns = correctAns;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "ResultScoreModel{" +
                "title='" + title + '\'' +
                ", totalQuestion='" + totalQuestion + '\'' +
                ", correctAns='" + correctAns + '\'' +
                ", percentage='" + percentage + '\'' +
                '}';
    }
}
